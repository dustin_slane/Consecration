﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Discord;
using Discord.WebSocket;

namespace Consecration
{
    /// <summary>
    /// Consecration
    /// 
    /// This is an application I have written in a very short amount of time
    /// that will clean up DM's between you and another person
    /// You can only delete messages YOU sent, but at least they won't be able to read that.
    /// 
    /// Dedicated to the Blue Dove.
    /// </summary>
    public partial class ConsecrationWindow : Window
    {
        private DiscordSocketClient _client = new DiscordSocketClient();
        private static List<IMessage> _cachedMessages = new List<IMessage>();
        private static string _recipient = "";
        private static int _channelType = 0;

        public ConsecrationWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Setup some event handlers
        /// </summary>
        /// <returns></returns>
        public async Task Initialize()
        {
            _client.Log += Log;
            _client.Ready += OnReady;

        }

        /// <summary>
        /// When discord is initialized
        /// </summary>
        /// <returns></returns>
        private async Task OnReady()
        {
            try
            {
                OnConnectButton.Dispatcher.Invoke(() =>
                {
                    OnConnectButton.Content =
                        $"Connected as {_client.CurrentUser.Username}#{_client.CurrentUser.Discriminator}";
                    ProgressBar.Visibility = Visibility.Hidden;
                });
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }

            FillDropdown();
        }

        private void SwitchContext()
        {
            if (DiscordDMDropdown == null || DiscordServerDropdown == null || DiscordGroupDMDropdown == null)
            {
                return;
            }
            DiscordDMDropdown.Visibility = Visibility.Collapsed;
            DiscordServerDropdown.Visibility = Visibility.Collapsed;
            DiscordGroupDMDropdown.Visibility = Visibility.Collapsed;

            // Discord Server
            if (_channelType == 0)
            {
                DiscordServerDropdown.Visibility = Visibility.Visible;
                DiscordServerDropdown.SelectedIndex = -1;
                return;
            }

            // Group DM
            if (_channelType == 1)
            {
                DiscordGroupDMDropdown.Visibility = Visibility.Visible;
                DiscordGroupDMDropdown.SelectedIndex = -1;
                return;
            }

            // Private DM
            if (_channelType == 2)
            {
                DiscordDMDropdown.Visibility = Visibility.Visible;
                DiscordDMDropdown.SelectedIndex = -1;

            }
        }

        internal class GroupDM
        {
            public ulong Id;
            public string Name;
            public SocketGroupChannel Channel;
        }

        /// <summary>
        /// Fill the dropdown with DM's to pick from
        /// </summary>
        private void FillDropdown()
        {
            try
            {
                var groupDms = _client.GroupChannels.Where(i=> i.Name != null).Select(i => new GroupDM { Id = i.Id, Name = i.Name, Channel = i }).ToList();
                var otherList = _client.GroupChannels.Where(i => i.Name == null).Select(i => new GroupDM
                {
                    Id = i.Id,
                    Name = string.Join(",",
                        i.Recipients.Select(r => r.Username).ToList()),
                    Channel = i
                });

                groupDms.AddRange(otherList);


                DiscordServerDropdown.Dispatcher.Invoke(() =>
                {
                    DiscordServerDropdown.Items.Clear();
                    DiscordServerDropdown.ItemsSource = _client.Guilds;
                    DiscordServerDropdown.DisplayMemberPath = "Name";
                    DiscordServerDropdown.SelectedValuePath = "Id";
                });

                DiscordGroupDMDropdown.Dispatcher.Invoke(() =>
                {
                    DiscordGroupDMDropdown.Items.Clear();
                    DiscordGroupDMDropdown.ItemsSource = groupDms;
                    DiscordGroupDMDropdown.DisplayMemberPath = "Name";
                    DiscordGroupDMDropdown.SelectedValuePath = "Id";
                });

                DiscordDMDropdown.Dispatcher.Invoke(() =>
                {
                    DiscordDMDropdown.Items.Clear();
                    DiscordDMDropdown.ItemsSource = _client.DMChannels;
                    DiscordDMDropdown.DisplayMemberPath = "Recipient.Username";
                    DiscordDMDropdown.SelectedValuePath = "Id";
                });

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        /// <summary>
        /// Spit it all to the console...
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        private async Task Log(LogMessage msg)
        {
            Console.WriteLine(msg.ToString());
        }

        private async void MainWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            await Initialize();
        }

        /// <summary>
        /// When a dropdown item is selecter, load messages in that conversation.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void DiscordDMDropdown_OnSelected(object sender, RoutedEventArgs e)
        {
            try
            {
                var box = (ComboBox) sender;
                if (box.SelectedItem is SocketDMChannel channel)
                {
                    _cachedMessages.Clear();

                    ProgressBar.IsIndeterminate = true;
                    ProgressBar.Visibility = Visibility.Visible;
                    MessagesFound.Text = $"Loading messages for {channel.Recipient.Username}";
                    _recipient = $"{channel.Recipient.Username}#{channel.Recipient.Discriminator}";

                    var messages = await channel.GetMessagesAsync().Flatten();
                    messages = messages.Where(i => i.Author.Id == _client.CurrentUser.Id);
                    bool looking = true;
                    while (looking && messages != null && messages.Any())
                    {
                        var m = await channel.GetMessagesAsync(messages.Last().Id, Direction.Before).Flatten();
                        if (m.Count() < 100)
                        {
                            looking = false;
                        }

                        _cachedMessages.AddRange(m.Where(i => i.Author.Id == _client.CurrentUser.Id));
                    }

                    ProgressBar.Visibility = Visibility.Hidden;

                    if (_cachedMessages != null)
                    {
                        MessagesFound.Text =
                            $"Found {_cachedMessages.Count()} messages between you and {channel.Recipient.Username}#{channel.Recipient.Discriminator}";
                    }
                    else
                    {
                        MessagesFound.Text =
                            $"Found 0 messages between you and {channel.Recipient.Username}#{channel.Recipient.Discriminator}";
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        /// <summary>
        /// We're connecting!
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void OnButtonConnect(object sender, RoutedEventArgs e)
        {
            if (_client.LoginState == LoginState.LoggedOut)
            {

                var token = DiscordTokenTextBox.Text;
                // Get rid of potential copy-paste problems
                token = token.Replace("\"", "").Trim();

                OnConnectButton.Dispatcher.Invoke(() =>
                {
                    OnConnectButton.Content =
                        $"Logging in...";
                    OnConnectButton.IsEnabled = false;
                    ProgressBar.IsIndeterminate = true;
                    ProgressBar.Visibility = Visibility.Visible;
                });

                try
                {
                    await _client.LoginAsync(TokenType.User, token);
                    await _client.StartAsync();
                }
                catch (Exception exx)
                {
                    MessageBox.Show(exx.Message);
                    OnConnectButton.Dispatcher.Invoke(() =>
                    {
                        OnConnectButton.Content =
                            $"Connect.";

                        OnConnectButton.IsEnabled = true;
                        ProgressBar.IsIndeterminate = false;
                        ProgressBar.Visibility = Visibility.Hidden;
                    });
                }
            }
        }

        /// <summary>
        /// ***Consecration***
        /// Instant                 9 sec cooldown
        /// Requires Paladin(Holy, Protection)
        /// Requires level 14
        /// Consecrates the land beneath you, causing
        /// [(30 % of Attack power) * 9] Holy damage over 
        /// 9 sec to enemies who enter the area.
        /// </summary>
        /// <returns></returns>
        private async Task Consecrate()
        {
            try
            {
                var messages = _cachedMessages.Where(i => i.Author.Id == _client.CurrentUser.Id).ToList();
                var count = messages.Count;
                ProgressBar.Maximum = count;
                ProgressBar.IsIndeterminate = false;

                ProgressBar.Value = 0;
                ProgressBar.Visibility = Visibility.Visible;
                foreach (var m in messages)
                {
                    await m.DeleteAsync();
                    await Task.Delay(100);
                    ProgressBar.Value++;
                }

                DiscordDMDropdown.SelectedIndex = -1;
                _cachedMessages = null;
                MessageBox.Show($"Done. Deleted {count} messages.");

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);

            }


        }

        private async void Consecrate_OnClick(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show($"ARE YOU SURE YOU WANT TO DELETE {_cachedMessages?.Count() ?? 0} MESSAGES\nFROM {_recipient}", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                
                await Consecrate();
            }
        }

        private void DiscordConversationTypeSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var box = (ComboBox)sender;
            _channelType = box.SelectedIndex;
            SwitchContext();
        }

        private async void DiscordServerDropdown_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var box = (ComboBox)sender;
                if (box.SelectedItem is SocketGuild guild)
                {
                    ProgressBar.IsIndeterminate = true;
                    ProgressBar.Visibility = Visibility.Visible;
                    MessagesFound.Text = $"Loading messages for {guild.Name}";
                    _recipient = $"{guild.Name}";
                    _cachedMessages.Clear();
                    var channels = _client.GetGuild(guild.Id).TextChannels;
                    foreach (var channel in channels)
                    {
                        var messages = new List<IMessage>();
                        var cm = await guild.GetTextChannel(channel.Id).GetMessagesAsync().Flatten();
                        var date = _client.GetGuild(guild.Id).Users.Where(u => u.Id == _client.CurrentUser.Id).First().JoinedAt ?? guild.CreatedAt;
                        

                        messages.AddRange(cm);
                        bool looking = true;
                        while (looking && messages.Any())
                        {
                            // get more messages
                            var m = await channel.GetMessagesAsync(messages.Last().Id, Direction.Before).Flatten();

                            // Ensure we don't get more messages then needed
                            if (!m.Any() || m.Last().CreatedAt < date)
                            {
                                looking = false;
                            }

                            messages.AddRange(m);
                        }

                        _cachedMessages.AddRange(messages.Where(m => m.Author.Id == _client.CurrentUser.Id).ToList());
                        ProgressBar.Visibility = Visibility.Hidden;

                        if (_cachedMessages != null)
                        {
                            MessagesFound.Text =
                                $"Found {_cachedMessages.Count()} messages you have sent to {guild.Name}";
                        }
                        else
                        {
                            MessagesFound.Text =
                                $"Found no messages sent to {guild.Name}";
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private async void DiscordGroupDMDropdown_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var box = (ComboBox)sender;
                if (box.SelectedItem is GroupDM channel)
                {
                    _cachedMessages.Clear();

                    ProgressBar.IsIndeterminate = true;
                    ProgressBar.Visibility = Visibility.Visible;

                    MessagesFound.Text = $"Loading messages for {channel.Name}";

                    var messages = await channel.Channel.GetMessagesAsync().Flatten();
                    _cachedMessages.AddRange(messages.Where(i => i.Author.Id == _client.CurrentUser.Id));
                    bool looking = true;
                    while (looking && messages != null && messages.Any())
                    {
                        var m = await channel.Channel.GetMessagesAsync(messages.Last().Id, Direction.Before).Flatten();
                        if (m.Count() < 100)
                        {
                            looking = false;
                        }

                        _cachedMessages.AddRange(m.Where(i => i.Author.Id == _client.CurrentUser.Id));
                    }

                    _cachedMessages.AddRange(messages);
                    ProgressBar.Visibility = Visibility.Hidden;

                    if (_cachedMessages != null)
                    {
                        MessagesFound.Text =
                            $"Found {_cachedMessages.Count()} messages between you and {channel.Name}";
                    }
                    else
                    {
                        MessagesFound.Text =
                            $"Found 0 messages between you and {channel.Name}";
                    }

                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
